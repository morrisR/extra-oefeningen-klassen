package be.multimedi.graphics.graphics.drawings;

import be.multimedi.graphics.graphics.Scaleable;

/**
 * @author Sven Wittoek
 * created on Friday, 21/01/2022
 */
public interface Drawable extends Scaleable {
    public abstract void draw(DrawingContext drawingContext);
}
