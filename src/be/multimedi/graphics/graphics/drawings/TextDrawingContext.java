package be.multimedi.graphics.graphics.drawings;

import be.multimedi.graphics.graphics.shapes.Circle;
import be.multimedi.graphics.graphics.shapes.Rectangle;
import be.multimedi.graphics.graphics.shapes.Triangle;

/**
 * @author Sven Wittoek
 * created on Friday, 21/01/2022
 */
public class TextDrawingContext implements DrawingContext{

    @Override
    public void draw(Rectangle rectangle) {
        System.out.println(rectangle);
    }

    @Override
    public void draw(Circle circle) {
        System.out.println(circle);
    }

    @Override
    public void draw(Triangle triangle) {
        System.out.println(triangle);
    }
}
