package be.multimedi.graphics.graphics.drawings;

import be.multimedi.graphics.graphics.shapes.Circle;
import be.multimedi.graphics.graphics.shapes.Rectangle;
import be.multimedi.graphics.graphics.shapes.Triangle;

/**
 * @author Sven Wittoek
 * created on Friday, 21/01/2022
 */
public interface DrawingContext {
    public abstract void draw(Rectangle rectangle);
    public abstract void draw(Circle circle);
    public abstract void draw(Triangle triangle);
}
