package be.multimedi.graphics.graphics.drawings;

/**
 * @author Sven Wittoek
 * created on Friday, 21/01/2022
 */
public class Drawing implements Drawable{
    private Drawable[] drawables;
    private int amountOfDrawables;
    private static final int DEFAULT_LENGTH = 10;

    public Drawing() {
        this.drawables = new Drawable[DEFAULT_LENGTH];
    }

    public void add(Drawable drawable) {
        amountOfDrawables++;
        drawables[findNextEmptyPosition()] = drawable;
    }

    public void remove(Drawable drawable) {
        for (int i = 0; i < drawables.length; i++) {
            if (drawables[i] == drawable) {
                amountOfDrawables--;
                drawables[i] = null;
                break;
            }
        }
    }

    public void clear() {
        drawables = new Drawable[DEFAULT_LENGTH];
        amountOfDrawables = 0;
    }

    public int getSize() {
        return amountOfDrawables;
    }

    public void scale(int scaleFactor) {
        for (Drawable drawable : drawables) {
            drawable.scale(scaleFactor);
        }
    }

    public void draw(DrawingContext drawingContext) {
        for (Drawable drawable : drawables) {
            if (drawable != null) {
                drawable.draw(drawingContext);
            }
        }
    }

    private int findNextEmptyPosition() {
        for (int i = 0; i < drawables.length; i++) {
            if (drawables[i] == null) {
                return i;
            }
        }
        int newIndex = drawables.length;
        enlargeDrawables();
        return newIndex;
    }

    private void enlargeDrawables() {
        Drawable[] newDrawables = new Drawable[drawables.length + 5];
        System.arraycopy(drawables, 0, newDrawables, 0, drawables.length);
        drawables = newDrawables;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder("This is a drawing with following shapes: \n");
        for (Drawable drawable : drawables) {
            if (drawable != null) {
                stringBuilder.append(drawable).append("\n");
            }
        }
        return stringBuilder.toString();
    }

}
