package be.multimedi.graphics.graphics;

import be.multimedi.graphics.graphics.drawings.Drawable;
import be.multimedi.graphics.graphics.drawings.DrawingContext;
import be.multimedi.graphics.lotto.Game;

/**
 * @author Sven Wittoek
 * created on Tuesday, 18/01/2022
 */
public abstract class Shape implements Drawable {
    private int x;
    private int y;

    public Shape() {
    }

    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setPosition(int x, int y) {
        setX(x);
        setY(y);
    }

    public abstract double getArea();
    public abstract double getPerimeter();
    public abstract void grow(int growthFactor);
}