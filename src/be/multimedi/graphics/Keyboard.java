package be.multimedi.graphics;

import java.util.Scanner;

/**
 * @author Sven Wittoek
 * created on Monday, 24/01/2022
 */
public class Keyboard {
    private static Scanner keyboard = new Scanner(System.in);

    private Keyboard() {
    }

    public static String nextln() {
        return keyboard.nextLine()
                .toLowerCase();
    }

    public static void close(){
        keyboard.close();
    }
}
