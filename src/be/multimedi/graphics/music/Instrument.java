package be.multimedi.graphics.music;

/**
 * @author Sven Wittoek
 * created on Monday, 24/01/2022
 */
public interface Instrument {
    void makeSound();
}
