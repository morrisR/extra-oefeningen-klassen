package be.multimedi.graphics.music;

import be.multimedi.graphics.Keyboard;

import java.util.Scanner;

/**
 * @author Sven Wittoek
 * created on Monday, 24/01/2022
 */
public class Musician {

    public void play() {
        /**
         * Maak een instantie van de interface Instrument, door er een anoniem geneste klasse van te maken
         * @see Instrument
         */
        Instrument instrument = new Instrument(){
            @Override
            public void makeSound() {
                System.out.println("Ieeeeee, I need to take more lessons...");
            }
        };
        /**
         * Voer ik de methode makeSound() uit van de lokaal geneste klasse aan de hand van de hierboven
         * aangemaakte instantie
         */
        instrument.makeSound();
    }
}
