package be.multimedi.graphics;

import be.multimedi.graphics.graphics.shapes.Rectangle;
import be.multimedi.graphics.lotto.Coin;
import be.multimedi.graphics.lotto.Game;
import be.multimedi.graphics.music.Musician;

/**
 * @author Sven Wittoek
 * created on Thursday, 20/01/2022
 */
public class Main {
    public static void main(String[] args) {
        Musician musician = new Musician();
        musician.play();
    }
}
