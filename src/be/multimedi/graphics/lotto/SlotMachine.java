//package be.multimedi.graphics.lotto;
//
//import javax.imageio.plugins.jpeg.JPEGQTable;
//import java.util.Arrays;
//import java.util.Random;
//import java.util.Scanner;
//
///**
// * @author Sven Wittoek
// * created on Tuesday, 18/01/2022
// */
//public class SlotMachine implements Game {
//    private static final int[] COINS_WON = {0, 10, 50, 100};
//    private int[] slots;
//
//    public SlotMachine() {
//        slots = new int[3];
//    }
//
//    public int play() {
//        generateRandomNumbers();
//        System.out.println("The slots halted on: ");
//        System.out.println(Arrays.toString(slots));
//        return calculateWonCoins();
//    }
//
//    private void generateRandomNumbers() {
//        Random random = new Random();
//        for (int i = 0; i < slots.length; i++) {
//            slots[i] = random.nextInt(6) + 1;
//        }
//    }
//
//    private int calculateWonCoins() {
//        int identicals = calculateWinningNumbers();
//        if (identicals == 3 && slots[0] == 3) {
//            return COINS_WON[3];
//        }
//        return COINS_WON[identicals];
//    }
//
//    private int calculateWinningNumbers() {
//        int identicalNumbers = 0;
//        for (int i = 0; i < slots.length; i++) {
//            for (int j = i + 1; j < slots.length; j++) {
//                if (i == j) {
//                    identicalNumbers++;
//                }
//            }
//        }
//        return identicalNumbers;
//    }
//
//    public int[] getPossibleWonCoins() {
//        return COINS_WON;
//    }
//}
