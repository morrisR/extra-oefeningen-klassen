package be.multimedi.graphics.lotto;

/**
 * @author Sven Wittoek
 * created on Friday, 21/01/2022
 */
public interface Game {

    Coin play();
    public default int[] getPossibleWonCoins(){
        return new int[]{0,5, 10, 20, 40, 80};
    }

    public static void printStatic() {
        System.out.println("Static printing form interface");
    }
}
