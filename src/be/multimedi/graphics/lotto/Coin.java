package be.multimedi.graphics.lotto;

/**
 * @author Sven Wittoek
 * created on Monday, 24/01/2022
 */
public interface Coin {

    int getAmountOfCoins();
}
