package be.multimedi.graphics.lotto;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * @author Sven Wittoek
 * created on Monday, 17/01/2022
 * Deze klasse representeerd het kansspel lotto.
 *
 */
public class Lotto implements Game {
    /**
     * Het aantal getrokken ballen in het spel
     */
    public static final int NUMBER_OF_BALLS = 6;
    /**
     * De random gegenereerde getrokken ballen
     */
    private int[] randomNumbers;
    /**
     * De gok van de speler
     */
    private int[] askedNumbers ;
    private Scanner keyboard;
    /**
     * Array met de waarden van het aantal coins die kunnen worden gewonnen.
     * Dit aantal zit ingesteld op de positie die het aantal geraden coins voorsteld.
     * bvb: bij 1 geraden getal zal het aantal gewonnen coins zich bevinden op index 1
     */
    private int[] coinsWon;

    /**
     * No argument constructor die ervoor zorgt dat alle arrays, het keyboard en de array met het aantal te winnen coins initialiseerd voor gebruik
     */
    public Lotto() {
        randomNumbers = new int[NUMBER_OF_BALLS];
        askedNumbers = new int[NUMBER_OF_BALLS];
        keyboard = new Scanner(System.in);
        coinsWon = new int[] {0, 5, 10, 20, 40, 80, 200};
    }

    public Coin play() {
        generateRandomNumbers();
        askForNumbers();

        int amountOfMatchingNumbers = calculateGuessedNumbers();
//        Lokaal geneste klasse LottoCoin, enkel en alleen bruikbaar binnen de play() methode.
        class LottoCoin implements Coin {
            private int amountOfCoins;

//            Getter die het aantal huidige coins terug geeft aan de hand van de amountOfCoins variabele.
            public int getAmountOfCoins() {
                return amountOfCoins;
            }

//            Methode die coins kan toevoegen
            public void addCoins(int amountToAdd) {
                amountOfCoins += amountToAdd;
            }

//            Methode die berekent hoeveel coins er zijn gewonnen en optelt met de reeds verzamelde coins
            public void win() {
                int coins = coinsWon[amountOfMatchingNumbers];
                amountOfCoins += coins;
            }
//            Hier eindigt de lokaal geneste klasse
        }

        System.out.println(Arrays.toString(randomNumbers));
        LottoCoin coin = new LottoCoin();

        return coin;
    }

    private int calculateGuessedNumbers() {
        int guessedNumbers = 0;
        for (int askedNumber: askedNumbers) {
            Arrays.sort(randomNumbers);
            if (Arrays.binarySearch(randomNumbers, askedNumber) >= 0) {
                guessedNumbers++;
            }
        }
        return guessedNumbers;
    }

    private void askForNumbers() {
        for (int i = 0; i < askedNumbers.length; i++) {
            int number;
            do {
                System.out.println("Please enter a number between 1 and 46");
                number = keyboard.nextInt();
            }while (!checkIfExistsInArray(askedNumbers, number));
            askedNumbers[i] = number;
        }
    }

    private void generateRandomNumbers() {
        Random random = new Random();
        for (int i = 0; i < randomNumbers.length; i++) {
            int randomNumber;
            do {
                randomNumber = random.nextInt(46) + 1;
            } while (!checkIfExistsInArray(randomNumbers, randomNumber));
            randomNumbers[i] = randomNumber;
        }
    }

    private boolean checkIfExistsInArray(int[] array, int number) {
        boolean doesNotContain = true;
        for (int arrayNumber : array) {
            if (number == arrayNumber) {
                doesNotContain = false;
                break;
            }
        }
        return doesNotContain;
    }
}
